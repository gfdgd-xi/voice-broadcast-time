#!/usr/bin/env python3
##########################################################################################
# 作者：gfdgd xi
# 版本：1.0.0
# 感谢：感谢 espeak 制作团队，提供了 espeak 给大家使用，让我能做这个程序
# 基于 Python3 的 tkinter 构建的语音播报器，使用 espeak 做朗读引擎
########################################################################################
import tkinter as tk
import tkinter.messagebox
import threading
import time
import os

# 获取星期数值对应的星期大写，范围为 0 ～ 6
def week_get(week):
    weeks = {'0': '日', '1': '一', '2': '二', '3': '三', '4': '四', '5': '五', '6': '六'}
    return weeks[week]

def day_string_get(second):
    # 需要用户提供秒，以判断是否为整，方便朗读
    year = time.strftime("%Y", time.localtime())
    month = int(time.strftime("%m", time.localtime()))
    day = int(time.strftime("%d", time.localtime()))
    hour = time.strftime("%H", time.localtime())
    minute = int(time.strftime("%M", time.localtime()))
    week = week_get(time.strftime("%w", time.localtime()))
    if minute == 0 and second == 0:
        speak_str = "整"
    elif second == 0:
        speak_str = "{}分".format(minute)
    else:
        speak_str = "{}分{}秒".format(minute, second)
    return "{}年{}月{}日{}时{}，星期{}".format(year, month, day, hour, speak_str, week)

def time_run():
    while True:
        minute = int(time.strftime("%M", time.localtime()))
        second = int(time.strftime("%S", time.localtime()))
        if minute == 30 or minute == 0:
            if second == 0:
                threadss = threading.Thread(target=speak_time, args=(0,))
                threadss.start()
        label1_text.set(day_string_get(second))
        time.sleep(1)


def speak_time(second):
    print("当前时间为{}".format(day_string_get(second)))
    os.system("espeak -v zh  '当前时间为{}'".format(day_string_get(second)))

def button1_cl():
    threads = threading.Thread(target=speak_time, args=((int(time.strftime("%S", time.localtime()))),))
    threads.start()

# 显示“关于这个程序”窗口
def about_this_program():
    tkinter.messagebox.showinfo(title="关于这个程序", message="一个基于 Python3 的 tkinter 制作的时间语音播报器\n版本：1.0.1\n适用平台：Linux")

# 创建和显示窗口
window = tk.Tk()
window.title("时间语音播报")
label1_text = tk.StringVar()
label1 = tk.Label(window, font=("Arial", 36), textvariable=label1_text)
button1 = tk.Button(window, text="朗读当前时间", command=button1_cl)
menu = tk.Menu(window)  # 设置菜单栏
programmenu = tk.Menu(menu, tearoff=0)  # 设置“程序”菜单栏
menu.add_cascade(label="程序", menu=programmenu)
programmenu.add_command(label="退出程序", command=window.quit)  # 设置“退出程序”项
help = tk.Menu(menu, tearoff=0)  # 设置“帮助”菜单栏
menu.add_cascade(label="帮助", menu=help)
help.add_command(label="关于这个程序", command=about_this_program)  # 设置“关于这个程序”项
# 添加控件
window.config(menu=menu)  # 显示菜单栏
label1.pack()
button1.pack()
thread = threading.Thread(target=time_run)
thread.start()
# 显示窗口
window.mainloop()