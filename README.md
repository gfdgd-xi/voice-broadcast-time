# 语音朗读器

#### 介绍
一个基于 Python3 的 tkinter 构建的语音朗读器，语音朗读引擎为 espeak

#### 软件架构
能运行 espeak 和 Python3 即可


#### 安装教程

1.  安装所需依赖

```
sudo apt install python3 python3-tk git espeak
```
2、下载该程序

```
git clone https://gitee.com/gfdgd-xi/voice-broadcast-time.git
```
3、运行该程序

```
cd voice-broadcast-time
chmod 777 main.py
./main.py
```




#### 使用说明

1. ……
2. ……
3. ……
（不想写）

#### 参与贡献

1. espeak 软件制作团队


#### 特技

好像无……

